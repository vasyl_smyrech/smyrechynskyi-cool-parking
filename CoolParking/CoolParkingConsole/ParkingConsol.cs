﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Threading.Tasks;
using CoolParking.DTOs;
using CoolParking.ConsoleApp.Helpers;
using CoolParking.ConsoleApp.Interfaces;
using CoolParking.ConsoleApp.Models;

namespace CoolParking.ConsoleApp
{
    public class ParkingConsole : IParkingConsole
    {
        private readonly IApiService _apiService;
        private string _processingVehicleRegistrationPlateNumber;
        private VehicleType? _processingVehicleType;
        private readonly OutputFormatter _outputFormatter;

        public ParkingConsole(IApiService apiService)
        {
            _apiService = apiService;
            _outputFormatter = new OutputFormatter();
        }

        public void Start()
        { 
            ShowMainMenu();
        }

        public void ShowMainMenu()
        {
            _outputFormatter.ClearConsole();
            _outputFormatter.CenteredText("Press digit for item chosing:");
            _outputFormatter.List(this.ShowMainMenu,
                ("Put a vehicle", async () => await ShowPutVehicleMenu()),
                ("Take a vehicle", async () => await ShowTakeVehicleMenu()),
                ("Top up a vehicle", async () => await ShowTopUpVehicleMenu()),
                ("Show free/occupied spaces", async () => await ShowFreeAndOccuriedSpaces()),
                ("List all vehicless at the parking", async () => await ShowAllVehiclesList()),
                ("Show income for current period", async () => await ShowIncomForCurrentPerriod()),
                ("Show parking balance", async () => await ShowParkingBalance()),
                ("Show current period transactions", async () => await ShowCurrentPerriodTransactions()),
                ("Show transactions archive", async () => await ShowTransactionsArchive()));
        }

        private async Task ShowPutVehicleMenu()
        {
            _outputFormatter.ClearConsole();
            _outputFormatter.CenteredText("Putting a vehicle to the parking");

            try
            {
                _processingVehicleRegistrationPlateNumber ??= await _apiService.GetRandomVehicleNumber();
                _outputFormatter.Info($"Your vehicle's registration plate number is " +
                $"{_processingVehicleRegistrationPlateNumber}");
            }
            catch (Exception)
            {
                _outputFormatter.Info("Your vehicles number was not defined automatically");
            }
            

            if (_processingVehicleType == null)
            {
                _outputFormatter.CenteredText("Press digit with your vehicle type:");
                _outputFormatter.List(
                    async () => await ShowPutVehicleMenu(),
                    ("Personal car", () => { _processingVehicleType = VehicleType.PassengerCar; }),
                    ("Truck", () => { _processingVehicleType = VehicleType.Truck; }),
                    ("Bus", () => { _processingVehicleType = VehicleType.Bus; }),
                    ("Motorcycle", () => { _processingVehicleType = VehicleType.Motorcycle; }),
                    ("Back to the main menu", this.ShowMainMenu));

                _outputFormatter.Info($"Your vehicle type is {_processingVehicleType}");

                var topUpSum = TopUpVehicleBalance();

                if (topUpSum != null)
                {
                    try
                    {
                        await _apiService.PostVehicle(new VehicleDto(
                            _processingVehicleRegistrationPlateNumber,
                            (int)_processingVehicleType.Value,
                            topUpSum.Value));
                    }
                    catch (Exception e)
                    {
                        _outputFormatter.Error(e.Message, this.ShowMainMenu);
                    }

                    _outputFormatter.ClearConsole();
                    _outputFormatter.CenteredText($"Your {_processingVehicleType.ToString().ToLower()} with registration number " +
                        $"{_processingVehicleRegistrationPlateNumber} balance is ${topUpSum}");

                    _outputFormatter.Quitable();
                    ClearProcessedVehicleData();
                }
            }
        }

        private async Task ShowTakeVehicleMenu()
        {
            try
            {
                var success = false;
                var vehicles = await _apiService.GetAllVehicles();
                while (!success)
                {
                    var result = await GetVehicleFromTable(async () => await ShowTakeVehicleMenu(),
                        vehicles, 
                        "Vehicle taking back",
                        "Enter your vehicle sequential number(the 'Num' column) and press 'Enter'");
                    var chosenVehicle = result.Item1;
                    success = !result.Item2;

                    if (chosenVehicle != null)
                    {
                        try
                        {
                            await _apiService.RemoveVehicle(chosenVehicle.Id);
                            _outputFormatter.Info($"You took you " +
                                $"{((VehicleType)chosenVehicle.VehicleType).ToString().ToLower()} " +
                                $"with registration number {chosenVehicle.Id}.");
                            _outputFormatter.Quitable();
                        }
                        catch (Exception e)
                        {
                            _outputFormatter.Error(e.Message);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                _outputFormatter.Error(e.Message);
            }
            finally
            {
                ShowMainMenu();
            }
        }

        private async Task ShowTopUpVehicleMenu()
        {
            try
            {
                var success = false;
                var vehicles = await _apiService.GetAllVehicles();

                while (!success)
                {
                    var result = await GetVehicleFromTable(async () => await ShowTopUpVehicleMenu(),
                        vehicles,
                        "Vehicle balance topping up",
                        "Enter your vehicle sequential number(the 'Num' column) and press 'Enter'");
                    var chosenVehicle = result.Item1;
                    success = result.Item2;

                    if (chosenVehicle != null)
                    {
                            _outputFormatter.ClearConsole();
                            var sum = TopUpVehicleBalance();
                            await _apiService.TopUpVehicle(new TopUpVehicleDto() { Id = chosenVehicle.Id, Sum = sum.Value });
                            _outputFormatter.Info($"You have toped up you " +
                                $"{((VehicleType)chosenVehicle.VehicleType).ToString().ToLower()} " +
                                $"with registration number {chosenVehicle.Id} with ${Math.Abs(sum.Value):0.##}");
                            _outputFormatter.Quitable();
                    }
                }
            }
            catch (Exception e)
            {
                _outputFormatter.Error(e.Message);
            }
            finally
            {
                ShowMainMenu();
            }
        }

        private async Task ShowFreeAndOccuriedSpaces()
        {
            _outputFormatter.ClearConsole();

            try
            {
                var capacity = await _apiService.GetParkingCapacity();
                var free = await _apiService.GetFreePlaces();
                _outputFormatter.CenteredText("Free spaces on the parking");
                _outputFormatter.Info($"Free spaces - {free} of {capacity}");
                _outputFormatter.Info($"Ocuppied spaces - {capacity - free} from {capacity}");
                _outputFormatter.Quitable();
            }
            catch (Exception e)
            {
                _outputFormatter.Error(e.Message);
            }
            finally
            {
                ShowMainMenu();
            }
        }

        private async Task ShowAllVehiclesList()
        {
            _outputFormatter.ClearConsole();

            try
            {
                var vehicles = await _apiService.GetAllVehicles();

                if (vehicles.Count > 0)
                {
                    _outputFormatter.VehiclesTable("All vehicles on the parking", vehicles, true);
                }
                else
                {
                    _outputFormatter.CenteredText("There aren't any vehicles at the parking");
                }
            }
            catch (Exception e)
            {
                _outputFormatter.Error(e.Message);
            }
            finally
            {
                _outputFormatter.Quitable();
                ShowMainMenu();
            }  
        }

        private async Task ShowIncomForCurrentPerriod()
        {
            _outputFormatter.ClearConsole();
            _outputFormatter.CenteredText("Income for the current period");

            try
            {
                var incomeForCurrentPeriod = await _apiService.GetParkingCurrentPeriodIncome();
                _outputFormatter.Info($"Parking income for the current period is ${incomeForCurrentPeriod}");
            }
            catch (Exception e)
            {
                _outputFormatter.Info(e.Message);
            }
            finally
            {
                _outputFormatter.Quitable();
                ShowMainMenu();
            } 
        }

        private async Task ShowParkingBalance()
        {
            _outputFormatter.ClearConsole();

            try
            {
                var balance = await _apiService.GetParkingBalance();
                _outputFormatter.CenteredText("Parking balance");
                _outputFormatter.Info($"Parking balance ${balance}");
                _outputFormatter.Quitable();
            }
            catch (Exception e)
            {
                _outputFormatter.Error(e.Message);
            }
            finally
            {
                ShowMainMenu();
            }
        }

        private async Task ShowCurrentPerriodTransactions()
        {
            _outputFormatter.ClearConsole();

            try
            {
                var transactions = await _apiService.GetLastTransactionsInfos();
                _outputFormatter.CenteredText("Current period transactions");

                if (transactions.Count == 0)
                {
                    _outputFormatter.CenteredText("Current transactions list is empty");
                }
                else
                {
                    foreach (var t in transactions)
                    {
                        _outputFormatter.Info($"Transaction date and time: {t.TransactionDate:MM/dd/yyyy HH:mm:ss}\n\t" +
                            $"Vehicle registration number: {t.VehicleId}\n\t" +
                            $"Sum withdraved from vehicle accaunt: $ {t.Sum: 0.00}");
                    }
                }

                _outputFormatter.Quitable();
            }
            catch (Exception e)
            {
                _outputFormatter.Error(e.Message);
            }
            finally
            {
                ShowMainMenu();
            }
        }

        private async Task ShowTransactionsArchive()
        {
            _outputFormatter.ClearConsole();

            try
            {
                var transactionsArchive = await _apiService.GetTransactionsInfosLogs();
                _outputFormatter.CenteredText("Transactions log archive");
                _outputFormatter.Info(transactionsArchive);
            }
            catch (Exception e)
            {
                _outputFormatter.Error(e.Message);
            }
            finally
            {
                _outputFormatter.Quitable();
                ShowMainMenu();
            }
        }

        private decimal? TopUpVehicleBalance()
        {
            _outputFormatter.CenteredText("Enter a top-up a sum and press the 'Enter' button");

            var wasSumParsed = false;
            var sum = 0.0m;

            while (!wasSumParsed || sum < 0)
            {
                wasSumParsed = Decimal.TryParse(_outputFormatter.Input("Sum in USD - $"), out sum);
                if (!wasSumParsed || sum < 0)
                {
                    _outputFormatter.CenteredText("You have entered invalid sum. Chose option:", Color.Red);
                    _outputFormatter.List(
                        ShowMainMenu,
                        ("Try again", null),
                        ("Back to main menu", ClearProcessedVehicleData));
                }
            }

            return wasSumParsed ? (decimal?)sum : null;
        }

        private async Task<(VehicleDto, bool)> GetVehicleFromTable(Action fallBack,
            List<VehicleDto> vehicles, string title, string info)
        {
            var success = false;
            _outputFormatter.ClearConsole();
            if (vehicles.Count > 0)
            {
                _outputFormatter.CenteredText(title);
                _outputFormatter.VehiclesTable(info, vehicles, false);
                _outputFormatter.Info("Tap 'q' then enter to quit into menu");
                var answer = _outputFormatter.Input("Num: ");

                if (Int32.TryParse(answer, out int answerInt))
                {
                    if (answerInt > 0 && answerInt <= vehicles.Count)
                    {
                        return (vehicles?[answerInt - 1], success);
                    }
                    else
                    {
                        _outputFormatter.Error("You have entered invalid data");
                        _outputFormatter.ClearConsole();
                        fallBack?.Invoke();
                    }
                }
                else if (Char.TryParse(answer, out char answerChar))
                {
                    if (answerChar == 'Q' || answerChar == 'q')
                    {
                        ShowMainMenu();
                    }
                    else
                    {
                        _outputFormatter.Error("You have entered invalid data");
                        _outputFormatter.ClearConsole();
                        await ShowTakeVehicleMenu();
                    }
                }
                else
                {
                    _outputFormatter.Error("You have entered invalid data");
                    _outputFormatter.ClearConsole();
                    fallBack?.Invoke();
                }
            }
            else
            {
                _outputFormatter.ClearConsole();
                _outputFormatter.CenteredText("There aren't any car at the parking");
                _outputFormatter.Quitable();
                ShowMainMenu();
            }

            return (null, success);
        }

        private void ClearProcessedVehicleData()
        {
            _processingVehicleRegistrationPlateNumber = null;
            _processingVehicleType = null;
            ShowMainMenu();
        }
    }
}
