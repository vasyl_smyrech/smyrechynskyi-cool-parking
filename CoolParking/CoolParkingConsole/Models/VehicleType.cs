﻿namespace CoolParking.ConsoleApp.Models
{
    public enum VehicleType { PassengerCar, Truck, Bus, Motorcycle }
}