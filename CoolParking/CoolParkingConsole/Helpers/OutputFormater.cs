﻿using System;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using Console = Colorful.Console;
using CoolParking.DTOs;
using CoolParking.ConsoleApp.Models;

namespace CoolParking.ConsoleApp.Helpers
{
    public class OutputFormatter
    {
        public void ClearConsole()
        {
            Console.Clear();
            CenteredText("Cool Parking");
        }

        public void CenteredText(string text)
        {
            CenteredText($"{text}\t", Color.Aqua);
        }

        public void CenteredText(string text, Color color)
        {
            var offset = string.Empty;
            var startTextPosition = (Console.WindowWidth - text.Length) / 2;
            for (var i = 0; i < startTextPosition; i++)
            {
                offset += " ";
            }
            Console.Write($"\n{offset}{text}\n\t", color);
        }

        public void Error(string errorMessage)
        {
            Error(errorMessage, null);
        }

        public void Error(string errorMessage, Action fallback)
        {
            Console.WriteLine($"\n\t{errorMessage}\n\tPress any key to proceed\n\t", Color.Red);
            Console.ReadKey();
            fallback?.Invoke();
        }

        public void Info(string info)
        {
            Console.Write($"\n\n\t{info}\n\t", Color.Aquamarine);
        }

        public string Input(string beforeInputText)
        {
            Console.Write($"\n\t{beforeInputText}", Color.Bisque);
            return Console.ReadLine();
        }

        public void List(Action fallback, params (string, Action)[] items)
        {
            var formatted = string.Empty;
            var additionalWitespaceIfmoreThenNineItems = items.Length > 9 ? " " : string.Empty;

            for (var i = 0; i < items.Length; i++)
            {
                formatted += $"\n\t{i + 1}{additionalWitespaceIfmoreThenNineItems} - {items[i].Item1}";
            }

            Console.Write($"{formatted}\n\t", Color.Bisque);
            if (items.Length < 10) {
                var answer = Console.ReadKey();
                var listLength = items.Length.ToString();
                Regex regex = new Regex($"[1-{listLength}]");

                if (!regex.IsMatch(answer.KeyChar.ToString()))
                {
                    ClearConsole();
                    fallback.Invoke();
                }

                try
                {
                    items[Int32.Parse(answer.KeyChar.ToString()) - 1].Item2.Invoke();
                }
                catch (Exception) 
                {
                    fallback?.Invoke();
                }
            }
        }

        public void Quitable()
        {
            Info("Press any key to proceed");
            Console.ReadKey();
        }

        public void VehiclesTable(string title,
            List<VehicleDto> vehicles, bool includeBalance)
        {
            CenteredText(title);
            Console.Write("\n");

            if(vehicles.Count > 0)
            {
                var titles = new string[] { "Num", "Reg. number", "Vehicle type", "Balance" };

                var dimOne = vehicles.Count + 1;
                var dimTwo = includeBalance ? 4 : 3;
                var tableData = new string[dimOne, dimTwo];
                for (var i = 0; i < dimOne; i++)
                {
                    for (var j = 0; j < dimTwo; j++)
                    {
                        if (i == 0)
                        {
                            tableData[0, j] = titles[j];
                        }
                        else
                        {
                            switch (j)
                            {
                                case 0:
                                    tableData[i, j] = i.ToString();
                                    break;
                                case 1:
                                    tableData[i, j] = vehicles[i - 1].Id;
                                    break;
                                case 2:
                                    tableData[i, j] = ((VehicleType)vehicles[i - 1].VehicleType).ToString();
                                    break;
                                case 3:
                                    tableData[i, j] = vehicles[i - 1].Balance.ToString();
                                    break;
                            }
                        }
                    }
                }

                var maxColumnsWidth = new int[dimTwo];

                for (var i = 0; i < dimTwo; i++)
                {
                    var maxItemLength = 0;

                    for (var j = 0; j < dimOne; j++)
                    {
                        maxItemLength = Math.Max(maxItemLength, tableData[j, i].Length);
                    }

                    maxColumnsWidth[i] = maxItemLength;
                }

                int sum = 0;
                Array.ForEach(maxColumnsWidth, delegate (int i) { sum += i; });

                var tableWidth = 1 + (3 * dimTwo) + sum;


                var offset = string.Empty;
                var tableOffset = (Console.WindowWidth - tableWidth) / 2;
                for (var i = 0; i < tableOffset; i++)
                {
                    offset += " ";
                }

                var divider = offset;
                for (var i = 0; i < tableWidth; i++)
                {
                    divider += "-";
                }

                for (var i = 0; i < dimOne; i++)
                {
                    if (i < 2)
                    {
                        Console.WriteLine(divider, Color.CadetBlue);
                    }

                    Console.Write($"{offset}|", Color.CadetBlue);

                    for (var j = 0; j < dimTwo; j++)
                    {
                        var redColored = j == 3 && i != 0 ? Decimal.Parse(tableData[i, j]) < 0 : false;
                        int itemLength = tableData[i, j].Length;
                        var tableItem = " ";
                        var diff = maxColumnsWidth[j] - tableData[i, j].Length;
                        for (var e = 0; e < diff; e++)
                        {
                            tableItem += " ";
                        }
                        tableItem += tableData[i, j];


                        Console.Write(tableItem, redColored? Color.Red : Color.Bisque);
                        Console.Write(" |", Color.CadetBlue);
                    }
                    Console.Write("\n");
                }

                Console.WriteLine(divider, Color.CadetBlue);
            }
        }
    }
}
