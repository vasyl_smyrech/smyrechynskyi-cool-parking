﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using CoolParking.ConsoleApp.Interfaces;
using CoolParking.DTOs;

namespace CoolParking.ConsoleApp.Services
{
    public class ApiService : IApiService
    {
        private readonly HttpClient _client;
        public ApiService(HttpClient httpClient)
        {
            _client = httpClient;
            _client.BaseAddress = new Uri("https://localhost:44351/api/");
            _client.DefaultRequestHeaders.Accept.Clear();
            _client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public async Task<decimal> GetParkingBalance()
        {
            var response = _client.GetAsync("parking/balance").Result;
            if (response.IsSuccessStatusCode)
            {
                var freePlacesJsonString = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<decimal>(freePlacesJsonString);
            }

            throw new Exception($"Response code - {response.StatusCode}" +
                $"\n\t{await response.Content.ReadAsStringAsync()}");
        }

        public async Task<int> GetFreePlaces()
        {
            var response = _client.GetAsync("parking/freePlaces").Result;
            if (response.IsSuccessStatusCode)
            {
                var freePlacesJsonString = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<int>(freePlacesJsonString);
            }

            throw new Exception($"Response code - {response.StatusCode}" +
                 $"\n\t{await response.Content.ReadAsStringAsync()}");
        }

        public async Task<int> GetParkingCapacity()
        {
            var response = _client.GetAsync("parking/capacity").Result;
            if (response.IsSuccessStatusCode)
            {
                var freePlacesJsonString = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<int>(freePlacesJsonString);
            }

            throw new Exception($"Response code - {response.StatusCode}" +
                $"\n\t{await response.Content.ReadAsStringAsync()}");
        }

        public async Task<List<VehicleDto>> GetAllVehicles()
        {
            var response = _client.GetAsync("vehicles").Result;
            if (response.IsSuccessStatusCode)
            {
                var allVehiclesJsonString = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<List<VehicleDto>>(allVehiclesJsonString);
            }

            throw new Exception($"Response code - {response.StatusCode}" +
                $"\n\t{await response.Content.ReadAsStringAsync()}");
        }

        public async Task<VehicleDto> GetVehicleById(string id)
        {
            var response = _client.GetAsync($"vehicles/{id}").Result;
            if (response.IsSuccessStatusCode)
            {
                var freePlacesJsonString = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<VehicleDto>(freePlacesJsonString);
            }

            throw new Exception($"Response code - {response.StatusCode}" +
                $"\n\t{await response.Content.ReadAsStringAsync()}");
        }

        public async Task<VehicleDto> PostVehicle(VehicleDto vehicle)
        {
            var response = _client.PostAsync("vehicles",
                new StringContent(JsonConvert.SerializeObject(vehicle),
                Encoding.UTF8,
                "application/json")).Result;

            if (response.IsSuccessStatusCode)
            {
                var freePlacesJsonString = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<VehicleDto>(freePlacesJsonString);
            }

            throw new Exception($"Response code - {response.StatusCode}" +
                 $"\n\t{await response.Content.ReadAsStringAsync()}");
        }

        public async Task<string> RemoveVehicle(string id)
        {
            var response = _client.DeleteAsync($"vehicles/{id}").Result;

            if (response.IsSuccessStatusCode)
            {
                var freePlacesJsonString = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<string>(freePlacesJsonString);
            }

            throw new Exception($"Response code - {response.StatusCode}" +
                $"\n\t{await response.Content.ReadAsStringAsync()}");
        }

        public async Task<VehicleDto> TopUpVehicle(TopUpVehicleDto topUpVehicleDto)
        {
            var response = _client.PutAsync("transactions/topUpVehicle",
                new StringContent(JsonConvert.SerializeObject(topUpVehicleDto),
                Encoding.UTF8,
                "application/json")).Result;

            if (response.IsSuccessStatusCode)
            {
                var freePlacesJsonString = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<VehicleDto>(freePlacesJsonString);
            }

            throw new Exception($"Response code - {response.StatusCode}" +
                 $"\n\t{await response.Content.ReadAsStringAsync()}");
        }

        public async Task<List<TransactionInfoDto>> GetLastTransactionsInfos()
        {
            var response = _client.GetAsync("transactions/last").Result;
            if (response.IsSuccessStatusCode)
            {
                var freePlacesJsonString = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<List<TransactionInfoDto>>(freePlacesJsonString);
            }

            throw new Exception($"Response code - {response.StatusCode}" +
                    $"\n\t{await response.Content.ReadAsStringAsync()}");
        }

        public async Task<string> GetTransactionsInfosLogs()
        {
            var response = _client.GetAsync("transactions/all").Result;
            if (response.IsSuccessStatusCode)
            {
                var freePlacesJsonString = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<string>(freePlacesJsonString);
            }

            throw new Exception($"Response code - {response.StatusCode}" +
                 $"\n\t{await response.Content.ReadAsStringAsync()}");
        }

        public async Task<string> GetRandomVehicleNumber()
        {
            var response = _client.GetAsync("vehicles/randomVehicleNumber").Result;
            if (response.IsSuccessStatusCode)
            {
                var randomJsonString = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<string>(randomJsonString);
            }

            throw new Exception($"Response code - {response.StatusCode}" +
                   $"\n\t{await response.Content.ReadAsStringAsync()}");
        }

        public async Task<decimal> GetParkingCurrentPeriodIncome()
        {
            var response = _client.GetAsync("parking/currentPeriodIncome").Result;
            if (response.IsSuccessStatusCode)
            {
                var currentPeriodIncomeJsonString = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<decimal>(currentPeriodIncomeJsonString);
            }

            throw new Exception($"Response code - {response.StatusCode}" +
                  $"\n\t{await response.Content.ReadAsStringAsync()}");
        }
    }
}
