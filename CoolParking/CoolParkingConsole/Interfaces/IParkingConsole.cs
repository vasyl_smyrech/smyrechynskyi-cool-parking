﻿namespace CoolParking.ConsoleApp.Interfaces
{
    public interface IParkingConsole
    {
        void Start();
    }
}
