﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CoolParking.DTOs;

namespace CoolParking.ConsoleApp.Interfaces
{
    public interface IApiService
    {
        Task<int> GetParkingCapacity();

        Task<decimal> GetParkingBalance();

        Task<int> GetFreePlaces();

        Task<List<VehicleDto>> GetAllVehicles();

        Task<VehicleDto> GetVehicleById(string id);

        Task<VehicleDto> PostVehicle(VehicleDto vehicle);

        Task<string> RemoveVehicle(string id);

        Task<VehicleDto> TopUpVehicle(TopUpVehicleDto topUpVehicleDto);

        Task<List<TransactionInfoDto>> GetLastTransactionsInfos();

        Task<string> GetTransactionsInfosLogs();

        Task<string> GetRandomVehicleNumber();

        Task<decimal> GetParkingCurrentPeriodIncome();
    }
}
