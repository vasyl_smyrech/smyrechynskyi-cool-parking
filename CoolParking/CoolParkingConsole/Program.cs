﻿using System;
using System.Net.Http;
using Microsoft.Extensions.DependencyInjection;
using CoolParking.ConsoleApp.Interfaces;
using CoolParking.ConsoleApp.Services;

namespace CoolParking.ConsoleApp
{
    public class Program
    {
        private static IServiceProvider _serviceProvider;

        static void Main(string[] _)
        {
            RegisterServices();

            var console = _serviceProvider.GetService<IParkingConsole>();
            console.Start();

            DisposeServices();
        }

        private static void RegisterServices()
        {
            var collection = new ServiceCollection()
                .AddSingleton<HttpClient>()
                .AddSingleton<IApiService>(p => new ApiService(new HttpClient()))
                .AddSingleton<IParkingConsole>(p => new ParkingConsole(
                    p.GetRequiredService<IApiService>()));

            _serviceProvider = collection.BuildServiceProvider();
        }

        private static void DisposeServices()
        {
            if (_serviceProvider == null)
            {
                return;
            }
            if (_serviceProvider is IDisposable)
            {
                ((IDisposable)_serviceProvider).Dispose();
            }
        }
    }
}
