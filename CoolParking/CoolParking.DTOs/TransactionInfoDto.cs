﻿using System;
using Newtonsoft.Json;

namespace CoolParking.DTOs
{
    public class TransactionInfoDto
    {
        public TransactionInfoDto() { }

        [JsonConstructor]
        public TransactionInfoDto(
            string vehicleId,
            decimal sum, 
            DateTime transactionDateTime)
        {
            VehicleId = vehicleId;
            Sum = sum;
            TransactionDate = transactionDateTime;
        }

        [JsonProperty("vehicleId")]
        public string VehicleId { get; set; }

        [JsonProperty("sum")]
        public decimal Sum { get; set; }

        [JsonProperty("transactionDate")]
        public DateTime TransactionDate { get; set; }
    }
}
