﻿using CoolParking.DTOs.Helpers;
using Newtonsoft.Json;

namespace CoolParking.DTOs
{
    public class TopUpVehicleDto
    {
        public TopUpVehicleDto() { }

        [JsonProperty("id", Required = Required.Always)]
        public string Id { get; set; }

        [JsonProperty("Sum", Required = Required.Always), 
            JsonConverter(typeof(StrictDecimalConverter))]
        public decimal Sum { get; set; }
    }
}
