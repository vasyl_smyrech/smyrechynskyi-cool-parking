﻿using CoolParking.DTOs.Helpers;
using Newtonsoft.Json;

namespace CoolParking.DTOs
{
    public class VehicleDto
    {
        public VehicleDto() { }

        [JsonConstructor]
        public VehicleDto(string id, int vehicleType, decimal balance)
        {
            Id = id;
            VehicleType = vehicleType;
            Balance = balance;
        }

        [JsonProperty("id", Required = Required.Always)]
        public string Id { get; set; }

        [JsonProperty("vehicleType", Required = Required.Always),
            JsonConverter(typeof(StrictIntConverter))]
        public int VehicleType { get; set; }

        [JsonProperty("balance", Required = Required.Always),
             JsonConverter(typeof(StrictDecimalConverter))]
        public decimal Balance { get; set; }
    }
}

