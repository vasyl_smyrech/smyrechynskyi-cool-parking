﻿using System;

namespace CoolParking.BL.Helpers
{
    internal static class RandomRegistrationPlateNumberGenerator
    {
        internal static string GenerateRandomRegistrationPlateNumber()
        {
            var lettersRange = (fromInclude: 'A', toInclude: 'Z');
            var digitsRange = (fromInclude: '0', toInclude: '9');

            return $"{GenerateCharactersInRange(2, lettersRange)}" +
                $"-{GenerateCharactersInRange(4, digitsRange)}" +
                $"-{GenerateCharactersInRange(2, lettersRange)}";
        }

        private static string GenerateCharactersInRange(int amount,
         (char fromInclude, char toInclude) charactersRange)
        {
            var random = new Random();
            var characters = string.Empty;
            var n = charactersRange.toInclude - charactersRange.fromInclude + 1;
            for (int i = 0; i < amount; i++)
            {
                char c = (char)(charactersRange.fromInclude + random.Next(n));
                characters += c;
            }
            return characters;
        }
    }
}
