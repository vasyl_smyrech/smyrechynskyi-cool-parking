﻿using CoolParking.BL.Models;

namespace CoolParking.BL.Helpers
{
    public static class VehicleValidators
    {
        public static bool IsVehicleIdValid(string id) => 
            Settings.VehicleNumberRegex.IsMatch(id);

        public static bool IsVehicleBalanceValid(decimal balance) => 
            balance >= Settings.MinSumWhenPutVehicle && balance <= decimal.MaxValue;

        public static bool IsVehicleTopUpSumValid(decimal balance) =>
            balance >= Settings.MinVehicleTopUpSum && balance <= decimal.MaxValue;  
    }
}
