﻿using System.Collections.Generic;
using CoolParking.BL.Models;

namespace CoolParking.BL.Helpers
{
    public class VehicleEqualityComparer : IEqualityComparer<Vehicle>
    { 
        public bool Equals(Vehicle x, Vehicle y)
        {
            if (x == null && y == null || x.Id == y.Id)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public int GetHashCode(Vehicle obj)
        {
            return obj.GetHashCode();
        }
    }
}
