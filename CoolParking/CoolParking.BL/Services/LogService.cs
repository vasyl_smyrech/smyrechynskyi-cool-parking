﻿using System;
using System.IO;
using CoolParking.BL.Interfaces;

namespace CoolParking.BL.Services
{
    public class LogService : ILogService
    {
        private StreamWriter _streamWriter;
        private StreamReader _streamReader;

        public LogService(string logFilePath)
        {
            LogPath = logFilePath;
        }

        public string LogPath { get; }

        public void Write(string logInfo)
        {
            try
            {
                _streamWriter = new StreamWriter(LogPath, true);
                _streamWriter.WriteLine(logInfo);
            }
            catch (Exception e)
            {
                throw new InvalidOperationException(e.Message);
            }
            finally
            {
                _streamWriter?.Dispose();
            }
        }

        public string Read()
        {
            try
            {
                _streamReader = new StreamReader(LogPath);
                return _streamReader.ReadToEnd();
            }
            catch (Exception e)
            {
                throw new InvalidOperationException(e.Message);
            }
            finally
            {
                _streamReader?.Dispose();
            }
        }
    }
}
