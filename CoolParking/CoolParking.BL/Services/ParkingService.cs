﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Helpers;
using CoolParking.BL.Models;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        private readonly ITimerService _withdrawTimer;
        private readonly ITimerService _logTimer;
        private readonly ILogService _logService;
        private readonly Parking _parking = Parking.Instance();

        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logService)
        {
            _withdrawTimer = withdrawTimer;
            _logTimer = logTimer;
            _logService = logService;
            withdrawTimer.Elapsed += WithdrawTimer_Elapsed;
            withdrawTimer.Start();
            logTimer.Elapsed += LogTimer_Elapsed;
            logTimer.Start();
        }

        public decimal GetBalance()
        {
            return _parking.Balance;
        }

        public int GetCapacity()
        {
            return _parking.Capacity;
        }

        public int GetFreePlaces()
        {
            return GetCapacity() - _parking.ParkedVehicles.Count;
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return new ReadOnlyCollection<Vehicle>(_parking.ParkedVehicles);
        }

        public void AddVehicle(Vehicle vehicle)
        {
            if (_parking.ParkedVehicles.AsParallel<Vehicle>().Contains<Vehicle>(vehicle, new VehicleEqualityComparer()))
            {
                throw new ArgumentException($"There is a vehicle with same registration number - " +
                    $"{vehicle.Id} between ones plased on parking. Shold I inform the police?");
            }

            if (_parking.ParkedVehicles.Count < Settings.ParkingCapacity)
            {
                _parking.ParkedVehicles.Add(vehicle);
            }
            else
            {
                throw new InvalidOperationException("You can't get parking service. " +
                    "All parking spaces have been already occupied");
            }
        }

        public void RemoveVehicle(string vehicleId)
        {
            try
            {
                var vehicle = GetVehicle(vehicleId);
                if (vehicle.Balance < 0.0m)
                {
                    throw new InvalidOperationException($"You must pay ${Math.Abs(vehicle.Balance):0.##} " +
                        $"as fees and penalties before taking your {vehicle.VehicleType.ToString().ToLower()} back.");
                }
                _parking.ParkedVehicles.Remove(vehicle);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            if (!VehicleValidators.IsVehicleTopUpSumValid(sum))
            {
                throw new ArgumentException("Negativ value can't be added to a vehicle balance");
            }

            try
            { 
                var vehicle = GetVehicle(vehicleId);
                vehicle.Balance += sum;
            }
            catch (Exception e)
            {
                throw new ArgumentException(e.Message);
            }
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            return _parking.TransactionsInfos.ToArray();
        }

        public string ReadFromLog()
        {
            try
            {
                return _logService.Read();
            } 
            catch (Exception)
            {
                throw;
            }
        }

        public void Dispose()
        {
            _withdrawTimer.Dispose();
            _logTimer.Dispose();
            _parking.ResetData();
        }

        private Vehicle GetVehicle(string vehicleId)
        {
            try
            {
                return _parking.ParkedVehicles.First(v => v.Id == vehicleId);
            }
            catch (Exception)
            {
                throw new ArgumentException($"There isn't vehicle with {vehicleId} registration number on the parking");
            }
        }

        private void LogTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs _)
        {
            string transactionsLogsForLastWritingPeriod = string.Empty;
            var transactionsInfos = _parking.TransactionsInfos;
            var elementCount = 0;

            try
            {
                foreach (var log in transactionsInfos)
                {
                    transactionsLogsForLastWritingPeriod += $"\t{log}\n";
                    elementCount++;
                }
            }
            catch (Exception)
            {
                return;
            }
           
            try
            {
                _logService.Write(transactionsLogsForLastWritingPeriod);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            TransactionInfo.RessetCurrentPeriodIncome();
            _parking.TransactionsInfos.RemoveRange(0, elementCount);
        }

        private void WithdrawTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            foreach (var vehicle in _parking.ParkedVehicles)
            {
                decimal feeAmauntIncludingPenalty;
                var feeAmaunt = Settings.VehiclesFees.GetValueOrDefault(vehicle.VehicleType);

                if (vehicle.Balance >= feeAmaunt)
                {
                    feeAmauntIncludingPenalty = feeAmaunt;
                } 
                else if (vehicle.Balance > 0)
                {
                    feeAmauntIncludingPenalty =
                        vehicle.Balance + (feeAmaunt - vehicle.Balance) * Settings.PenaltyRatio;
                }
                else
                {
                    feeAmauntIncludingPenalty = feeAmaunt * Settings.PenaltyRatio;
                }

                vehicle.Balance -= feeAmauntIncludingPenalty;
                _parking.Balance += feeAmauntIncludingPenalty;
                _parking.TransactionsInfos.Add(new TransactionInfo(
                    vehicle.Id, feeAmaunt, _parking.Balance, vehicle.Balance));
            }
        }
    }
}