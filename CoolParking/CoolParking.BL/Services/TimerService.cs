﻿using System;
using System.Timers;
using CoolParking.BL.Interfaces;

namespace CoolParking.BL.Services
{
    public class TimerService : ITimerService
    {
        public event ElapsedEventHandler Elapsed;
        private Timer _timer;

        public TimerService(double interval)
        {
            Interval = interval;
            SetTimer();
        }

        public double Interval { get; set; }

        public void Start()
        {
            _timer.Start();
        }

        public void Stop()
        {
            _timer.Stop();
        }
        public void Dispose()
        {
            _timer.Dispose();
        }

        private void SetTimer()
        {
            _timer = new Timer(Interval)
            {
                AutoReset = true,
                Enabled = true
            };
            _timer.Elapsed += OnTimedEvent;
        }

        private void OnTimedEvent(Object source, ElapsedEventArgs e)
        {
            Elapsed?.Invoke(this, null);
        }
    }
}