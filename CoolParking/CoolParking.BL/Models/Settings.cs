﻿using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models
{
    public static class Settings
    {
        public static string LogFilePath 
            => $@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\Transactions.log";

        public static Regex VehicleNumberRegex => new Regex("[A-Z]{2}-[0-9]{4}-[A-Z]{2}");

        public static decimal MinSumWhenPutVehicle => 0.0m;

        public static decimal MinVehicleTopUpSum => 0.0m;

        public static double FeeWithdrawalPeriodInMiliseconds => 5000;

        public static double LogWritingPeriodInMiliseconds => 60000;

        public static int ParkingCapacity => 10;

        public static decimal PenaltyRatio => 2.5m;

        public static decimal StartBalance => 0.0m;

        public static Dictionary<VehicleType, decimal> VehiclesFees { get; } 
            = new Dictionary<VehicleType, decimal>()
        {
            { VehicleType.Bus, 3.5m },
            { VehicleType.Motorcycle, 1.0m },
            { VehicleType.PassengerCar, 2.0m },
            { VehicleType.Truck, 5.0m }
        };
    }
}
