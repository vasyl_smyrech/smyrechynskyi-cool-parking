﻿using System;
using CoolParking.BL.Helpers;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            if (!VehicleValidators.IsVehicleIdValid(id))
            {
                throw new ArgumentException($"RegistrationPlateNumber must be in the 'ХХ-YYYY-XX' format." +
                    "\nWhere 'X' - letter(A-Z), Y - digit(0-9)");
            }

            if (!VehicleValidators.IsVehicleBalanceValid(balance))
            {
                throw new ArgumentException("Vehicle balance can't be negative");
            }

            Id = id;
            VehicleType = vehicleType;
            Balance = balance;
        }

        public string Id { get; }

        public VehicleType VehicleType { get; }

        public decimal Balance { get; internal set; }

        public static string GenerateRandomRegistrationPlateNumber()
        {
            return RandomRegistrationPlateNumberGenerator.GenerateRandomRegistrationPlateNumber();
        }
    }
}