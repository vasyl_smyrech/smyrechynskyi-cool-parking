﻿using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public class Parking
    {
        private static Parking instance;

        private Parking()
        {
            ResetData();
        }

        public static Parking Instance()
        {
            return instance ??= new Parking();
        }

        public decimal Balance { get; set; }

        public int Capacity { get; set; }

        public List<TransactionInfo> TransactionsInfos { get; set; }

        public List<Vehicle> ParkedVehicles { get; private set; }

        internal void ResetData()
        {
            Balance = Settings.StartBalance;
            Capacity = Settings.ParkingCapacity;
            ParkedVehicles  = new List<Vehicle>();
            TransactionsInfos = new List<TransactionInfo>();
        }
    }
}