﻿using System;

namespace CoolParking.BL.Models
{
    public struct TransactionInfo
    {
        public TransactionInfo(
            string vehicleId,
            decimal sum, 
            decimal parkingBalanceAfterTA,
            decimal vehicleBalanceAfterTA)
        {
            VehicleId = vehicleId;
            Sum = sum;
            ParkingBalanceAfterTA = parkingBalanceAfterTA;
            VehicleBalanceAfterTA = vehicleBalanceAfterTA;
            CreationDateTime = DateTime.Now;
            ParkingCurrentPeriodIncome += sum;
        }

        public static decimal ParkingCurrentPeriodIncome { get; private set; }

        public string VehicleId { get; }

        public decimal Sum { get; }

        public decimal ParkingBalanceAfterTA { get; }

        public decimal VehicleBalanceAfterTA { get; }

        public DateTime CreationDateTime { get; }

        internal static void RessetCurrentPeriodIncome()
        {
            ParkingCurrentPeriodIncome = 0.0m;
        }

        override public string ToString()
        {
            return $"Transaction date and time: {CreationDateTime:MM/dd/yyyy HH:mm:ss}\n\t" +
                $"Vehicle registration number: {VehicleId}\n\t" +
                $"Sum withdraved from vehicle accaunt: $ {Sum: 0.00}\n\t" +
                $"Parking balance after transaction: $ {ParkingBalanceAfterTA: 0.00}\n\t" +
                $"Vehicle balance after transaction: $ {VehicleBalanceAfterTA: 0.00}\n\t";
        }

    }
}