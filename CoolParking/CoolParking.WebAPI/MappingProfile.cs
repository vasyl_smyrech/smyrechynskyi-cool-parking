﻿using AutoMapper;
using CoolParking.BL.Models;
using CoolParking.DTOs;

namespace CoolParking.WebAPI
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Vehicle, VehicleDto>()
              .ReverseMap();

            CreateMap<TransactionInfo, TransactionInfoDto>()
              .ReverseMap();
        }
    }
}
