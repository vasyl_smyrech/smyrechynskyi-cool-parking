﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using CoolParking.BL.Helpers;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.DTOs;

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TransactionsController : ControllerBase
    {
        private readonly IParkingService _parkingService;

        public TransactionsController(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }

        [HttpGet("last")]
        public ActionResult<List<TransactionInfoDto>> GetLastTransactionsInfos()
        {
            var transactions = _parkingService.GetLastParkingTransactions()
                .Select(x => new TransactionInfoDto(
                    x.VehicleId,
                    x.Sum,
                    x.CreationDateTime)
                ).ToList();
            return Ok(transactions);
        }

        [HttpGet("all")]
        public ActionResult<string> GetAllTransactionsInfos()
        {
            try
            {
                return Ok(_parkingService.ReadFromLog());
            }
            catch (Exception e)
            {
                return NotFound(e);
            }
        }

        [HttpPut("topUpVehicle")]
        public ActionResult<VehicleDto> TopUpVehicle(TopUpVehicleDto topUpVehicleDto)
        {
            if (!VehicleValidators.IsVehicleIdValid(topUpVehicleDto.Id))
            {
                return BadRequest($"The '{topUpVehicleDto.Id}' isn't valid vehicle's id");
            }
            
            if (!VehicleValidators.IsVehicleTopUpSumValid(topUpVehicleDto.Sum))
            {
                return BadRequest($"Invalid top up sum. Top up sum must be more or equal " +
                    $"${Settings.MinVehicleTopUpSum} and less than ${decimal.MaxValue}");
            }

            try
            {
                _parkingService.TopUpVehicle(topUpVehicleDto.Id, topUpVehicleDto.Sum);
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }

            return Ok(_parkingService.GetVehicles().FirstOrDefault(v => v.Id == topUpVehicleDto.Id));
        }
    }
}