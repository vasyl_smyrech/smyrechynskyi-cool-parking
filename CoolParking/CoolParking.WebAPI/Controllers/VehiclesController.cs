﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using CoolParking.BL.Helpers;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.DTOs;
using AutoMapper;

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class VehiclesController : ControllerBase
    {
        private readonly IParkingService _parkingService;
        private readonly IMapper _mapper;

        public VehiclesController(IParkingService parkingService, IMapper mapper)
        {
            _parkingService = parkingService;
            _mapper = mapper;
        }

        [HttpGet]
        public ActionResult<List<VehicleDto>> Get()
        {
            return Ok(_parkingService.GetVehicles()
                 .Select(x => _mapper.Map<Vehicle, VehicleDto>(x)).ToList());
        }

        [HttpGet("{id}")]
        public ActionResult<VehicleDto> GetVehicleById(string id)
        {
            if (!VehicleValidators.IsVehicleIdValid(id))
            {
                return BadRequest($"The '{id}' isn't valid vehicle's id");
            }

            var vehicle = _parkingService
                .GetVehicles()
                .FirstOrDefault(v => v.Id == id);

            if (vehicle == null)
            {
                return NotFound($"There aren't any vehicle with '{id}' plate number");
            }

            return Ok(_mapper.Map<Vehicle, VehicleDto>(vehicle));
        }

        [HttpGet("randomVehicleNumber")]
        public ActionResult<string> GetRandomVehicleNumber()
        {
            return Ok(Vehicle.GenerateRandomRegistrationPlateNumber());
        }

        [HttpPost]
        public ActionResult<VehicleDto> PostVehicle(VehicleDto vehicleDto)
        {
            try
            {
                if (vehicleDto.VehicleType > Enum.GetNames(typeof(VehicleType)).Length - 1
                    || vehicleDto.VehicleType < 0)
                {
                    throw new ArgumentException($"Can't convert {vehicleDto.VehicleType} to {nameof(VehicleType)}");
                }

                var vehicle = _mapper.Map<VehicleDto, Vehicle>(vehicleDto);
                _parkingService.AddVehicle(vehicle);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

            return Created(string.Empty, vehicleDto);
        }

        [HttpDelete("{id}")]
        public ActionResult RemoveVehicle(string id)
        {
            if (!VehicleValidators.IsVehicleIdValid(id))
            {
                return BadRequest($"The '{id}' isn't valid vehicle's id");
            }

            try
            {
                _parkingService.RemoveVehicle(id);
            }
            catch (ArgumentException e)
            {
                return NotFound(e.Message);
            }
            catch (InvalidOperationException e)
            {
                return BadRequest(e.Message);
            }

            return NoContent();
        }
    }
}